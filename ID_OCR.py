# python3 ID_OCR.py -i rotated12.png
from text_extraction import text_extraction_best as text_extract
from digit_extraction import digit_extraction_full as digits_extract
from cv2 import cv2
import os
import numpy as np
import datetime
from skimage.measure import compare_ssim
import json
import argparse
import re
import sys

# Initialize variables needed
unsorted = []
currentScore = 0
currentBoundaries = []
boundaries = [([0,0,0],[128,130,120]),([0,0,0],[90,90,90])] #[123,135,123] 
Threshed = []
count = 0
Text = []
jsonText = []

# Initialize the regions of interest with their dynamic ranges 
RoIDim = [0.5571345029239766,0.7994736842105263,0.4538709677419355,0.9693548387096774]
CoBDim = [[0.4898854568854569,0.584015444015444,0.0957446808510638,0.6748387096774194],[0.5225102319236016,0.5907230559345157,0.0777446808510638,0.6748387096774194]]
DoIDim = [[0.6471345029239766,0.7594736842105263,0.5338709677419355,0.9093548387096774],[0.6971350613915416,0.7667121418826739,0.5038709677419355,0.8893548387096774]]
DoBDim = [[0.5644015444015444,0.6625146198830409,0.0957446808510638,0.4553896832084727],[0.5675306957708049,0.6493860845839018,0.5038709677419355,0.8893548387096774]]
NameDim = [[0.3559073359073359,0.4782754182754183,0.0957446808510638,0.9448387096774194],[0.3410641200545703,0.461118690313779,0.0777446808510638,0.9448387096774194]]
SurnameDim = [[0.254826254826254,0.3538011695906433,0.0797872340425532,0.9448387096774194],[0.2223738062755798,0.3410641200545703,0.0777872340425532,0.9448387096774194]]

# Initialize the argeparse for the required inputs
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="first input image")
args = vars(ap.parse_args())

# Start the Extraction of RoIs from the Whole ID book
def Extract_ROIs(image, Type):
    # Get ID width and height
    (h, w) = image.shape[:2]

    #Extract and Write CoB
    CoBPic = image[round(int(h)*CoBDim[Type][0]) : round(int(h)*CoBDim[Type][1]), round(int(w)*CoBDim[Type][2]) : round(int(w)*CoBDim[Type][3])]

    #Extract and Write DoIs
    DoIPic = image[round(int(h)*DoIDim[Type][0]) : round(int(h)*DoIDim[Type][1]), round(int(w)*DoIDim[Type][2]) : round(int(w)*DoIDim[Type][3])]

    #Extract and Write DoBs
    DoBPic = image[round(int(h)*DoBDim[Type][0]) : round(int(h)*DoBDim[Type][1]), round(int(w)*DoBDim[Type][2]) : round(int(w)*DoBDim[Type][3])]

    #Extract and Write Names
    NamePic = image[round(int(h)*NameDim[Type][0]) : round(int(h)*NameDim[Type][1]), round(int(w)*NameDim[Type][2]) : round(int(w)*NameDim[Type][3])]

    #Extract and Write SurNames
    SurnamePic = image[round(int(h)*SurnameDim[Type][0]) : round(int(h)*SurnameDim[Type][1]), round(int(w)*SurnameDim[Type][2]) : round(int(w)*SurnameDim[Type][3])]

    ROIs = [CoBPic, DoIPic, DoBPic, NamePic, SurnamePic]

    return ROIs

# Threshold the ROIs using colour coundaries
def ROI_Thresh(ROIs, Type):
    boundary = []
    for file in ROIs:
        imgLoad = file
        boundary.append(boundaries[Type])
        for (lower, upper) in boundary:
            # create NumPy arrays from the boundaries
            lower = np.array(lower, dtype = 'uint8')
            upper = np.array(upper, dtype = 'uint8')

            (h,w) = imgLoad.shape[:2]
            blank_image = np.zeros((h,w,3), np.uint8)

            # find the colors within the specified boundaries and apply the mask
            mask = cv2.inRange(imgLoad, lower, upper)
            img = cv2.bitwise_not(imgLoad, blank_image, mask = mask)

            # Threshold RoI for sorting
            ret, img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV)

        # cv2.imshow('img',img)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        # kernel = kernel = np.ones((3,3),np.uint8)
        # img = cv2.bitwise_not(img)
        # img = cv2.erode(img, kernel, iterations=1)
        # img = cv2.bitwise_not(img)
        # cv2.imshow('img',img)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        Threshed.append(img)
    return Threshed

# Extract the text from the threshed ROIs
def Extract_Text(Threshed, type):
    count = 0
    for file in Threshed:
        # Use when needing to look at threshed image for validations
        # cv2.imshow('img', file)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        count += 1
        # load the example image and convert it to grayscale
        if (count == 1) or (count == 4) or (count == 5):
            #OCR the Text from the image given
            Name = text_extract(file, type)
            print(Name)
            match = re.search(r'\n', Name, re.M)
            if match is not None:
                matchregs = match.regs[0][1]
                n = Name[matchregs]
                if n == '\n':
                    matchregs = matchregs+1
                Name = Name[matchregs:]

            #Remove anything after the image given
            match = re.search(r'\n', Name, re.M)
            if match is not None:
                matchregs = match.regs[0][1]
                n = Name[matchregs]
                if n == '\n':
                    matchregs = matchregs-1
                Name = Name[:matchregs]
                length = len(Name)
                if length == 0 or length == 1:
                    Text.append('Could not Read Date')
                    continue

            if Name is not '':
                Text.append(Name)
            else:
                Text.append('Could Not Read Text')
        else:
            #OCR the Dates from the image given
            # digits = tool.image_to_string(img, lang='eng', builder=po.tesseract.DigitBuilder())
            digits = digits_extract(file, type)
            # find length of digits string
            print(digits)
            length = len(digits)
            # Move onto the next one if cannot find any digits
            if length == 0 or length == 1:
                Text.append('Could not Read Date')
                continue

            # Understand how many characters were found and organise them into their matching string
            if digits is not '':
                if len(digits) == 10:
                        digits = digits[:4]+ '-' + digits[5:7] + '-' + digits[8:]
                if len(digits) == 9:
                    if digits[4] == '7' or digits[4] == '2':
                        digits = digits[:4]+ '-' + digits[5:7] + '-' + digits[7:]
                    if digits[6] == '7' or digits[6] == '2':
                        digits = digits[:4]+ '-' + digits[4:6] + '-' + digits[7:]
                if len(digits) == 8:
                    digits = digits[:4]+ '-' + digits[4:6] + '-' + digits[6:]

                Text.append(digits)

            else:
                Text.append('Could not Read Date')
    return Text

#Read in image in grayscale
imgLoad = cv2.imread(args["image"])

#find shape of image
(h, w) = imgLoad.shape[:2]

#Extract and Write DoIs
img = imgLoad[round(int(h)*RoIDim[0]) : round(int(h)*RoIDim[1]), round(int(w)*RoIDim[2]) : round(int(w)*RoIDim[3])]

img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#Convert image to treshold image
ret, img = cv2.threshold(img, 100, 255, cv2.THRESH_BINARY)

#find shape of ROI
(h, w) = img.shape[:2]

# Create blank image with the same size as the given image
img2 = np.zeros((h,w,3), np.uint8)
img2[0:h,0:w] = (255,255,255)
img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

# Threshold the blank template
ret, img2 = cv2.threshold(img2, 100, 255, cv2.THRESH_BINARY)

# resize the template
img2 = cv2.resize(img2, (w, h))

# Compare the differences of the blank image and the ROI
(score, diff) = compare_ssim(img, img2, full=True, multichannel=True)
diff = (diff * 255).astype("uint8")

#split into the different functionalities for type 1 and type 2
if score > 0.9:
    ROIs = Extract_ROIs(imgLoad, 0)
    Threshed = ROI_Thresh(ROIs, 0)
    Text = Extract_Text(Threshed, 0)
else:
    ROIs = Extract_ROIs(imgLoad, 1)
    Threshed = ROI_Thresh(ROIs, 1)
    Text = Extract_Text(Threshed, 1)

# Used to create a jsonString for API calls
count = 0
for object in Text:
    if count == 0:
        jsonText.append(['Country', object])
    if count == 1:
        jsonText.append(['DoI', object])
    if count == 2:
        jsonText.append(['DoB', object])
    if count == 3:
        jsonText.append(['Name', object])
    if count == 4:
        jsonText.append(['Surname', object])
    count += 1
jsonText.append(['DateTime', str(datetime.datetime.now())])

# Encode in json format to pass back
Encoded_Text = json.dumps(jsonText)
print(Encoded_Text)
