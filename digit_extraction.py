# import the necessary packages
from keras.preprocessing.image import img_to_array
from keras.models import load_model
from pyimagesearch.utils.captchahelper import preprocess
from imutils import contours
from imutils import paths
import numpy as np
import argparse
import imutils
from cv2 import cv2


model = load_model('output/lenet.hdf5')

	
def digit_extraction_full(image, type):
	# Newer IDs are only is smaller/ less quality sizes in my image set
	if type == 0:
		image = cv2.resize(image, (0,0), fx=2.5,fy=2.5)
	if type == 1:
		image = cv2.resize(image, (0,0), fx=2,fy=2)

	# Find the shape and ensure that the size has depth, else do not gray (causes errors)
	shape = image.shape
	if len(shape) == 3:
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	else:
		gray = image

	# Make borders around the image to make sure none of the characters are cut off
	gray = cv2.copyMakeBorder(gray, 20, 20, 20, 20,
		cv2.BORDER_CONSTANT, value=[255,255,255])

	# threshold the image to reveal the digits
	thresh = cv2.threshold(gray, 0, 255,
		cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

	# find contours in the image, keeping only the four largest ones,
	# then sort them from left-to-right
	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
	cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
	cnts = contours.sort_contours(cnts)[0]

	# initialize the output image as a "grayscale" image with 3
	# channels along with the output predictions
	output = cv2.merge([gray] * 3)
	predictions = []
	hts = []
	characters = []
	if cnts != []:
		cnts = contours.sort_contours(cnts)[0]
		# loop over the contours
		for c in cnts:
			# compute the bounding box for the contour then extract the
			# digit
			(x, y, w, h) = cv2.boundingRect(c)
			roi = gray[y - 5:y + h + 5, x - 5:x + w + 5]
			if roi.size == 0:
				continue

			# Only use this for testing purposes
			# cv2.imshow('img', roi)
			# cv2.waitKey(0)
			# cv2.destroyAllWindows()

			height,width = roi.shape[:2]
			rois = []

			# If width is greater than the height, there is a potential
			# two digits are touching and need to be divided into two
			# and checked again
			if width > height:
				# Get the two new images with potential digits
				width1 = round(width/2)
				roi1 = roi[0:height, 0:width1]
				roi2 = roi[0:height, width1:width1 + width]
				roi1 = cv2.copyMakeBorder(roi1, 5, 5, 5, 5,
					cv2.BORDER_CONSTANT, value=[255,255,255])
				roi2 = cv2.copyMakeBorder(roi2, 5, 5, 5, 5,
					cv2.BORDER_CONSTANT, value=[255,255,255])
				rois.append(roi1)
				rois.append(roi2)

				# Only use this for testing purposes
				# cv2.imshow('roi1', roi1)
				# cv2.imshow('roi2', roi2)
				# cv2.waitKey(0)

				cv2.destroyAllWindows()
				count = 0
				for r in rois:
					# pre-process the ROI and classify it then classify it
					r = preprocess(r, 28, 28)
					r = np.expand_dims(img_to_array(r), axis=0)
					pred = model.predict(r).argmax(axis=1)[0]
					if count == 0:
						predictions.append(str(pred))
						hts.append([[h,y,y+h]])

						# draw the prediction on the output image
						cv2.rectangle(output, (x - 2, y - 2),
							(x + int(w/2) + 4, y + h + 4), (0, 255, 0), 1)
						cv2.putText(output, str(pred), (x - 5, y - 5),
							cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)

					if count == 1:
						predictions.append(str(pred))
						hts.append([[h,y,y+h]])

						# draw the prediction on the output image
						cv2.rectangle(output, (x - 2, y - 2),
							(x+int(w/2) + w + 4, y + h + 4), (0, 255, 0), 1)
						cv2.putText(output, str(pred), (x+int(w/2) - 5, y - 5),
							cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)

					count += 1
			else:
				# pre-process the ROI and classify it then classify it
				roi = preprocess(roi, 28, 28)
				roi = np.expand_dims(img_to_array(roi), axis=0)
				pred = model.predict(roi).argmax(axis=1)[0]
				predictions.append(str(pred))
				hts.append([[h,y,y+h]])

				# draw the prediction on the output image
				cv2.rectangle(output, (x - 2, y - 2),
					(x + w + 4, y + h + 4), (0, 255, 0), 1)
				cv2.putText(output, str(pred), (x - 5, y - 5),
					cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)
	
	# Only use this for testing purposes
	cv2.imshow("Output", output)
	cv2.waitKey(0)
	cv2.destroyAllWindows()

	height = []
	highest = 0
	start = []
	end = []
	counter = 0

	# Calculate height median of the text characters
	for ht in hts:
		h = ht[0][0]
		height.append(h)
		counter += 1
		hs = ht[0][1]
		he = ht[0][2]
		start.append(hs)
		end.append(he)
	sortd = sorted(height.copy(), reverse = True)
	highest = sortd[1]

	# Calculate the start and end points for the characters that fit into the median height
	count = 0
	starter = 0
	ender = 0
	for h in height:
		s = start[count]
		e = end[count]
		if h == highest:
			starter = s
			ender = e
			break
		count += 1

	# Match those characters that fit into the height range and start and end point ranges
	count = 0
	hts1 = []
	for h in height:
		s = start[count]
		e = end[count]
		if (s >= starter - 5 and s <= ender + 5) and (e >= starter - 5 and e <= ender + 5):
			characters.append(predictions[count])
			hts1.append(hts[count])
		count += 1

	finalHeights = []
	finalCharacters = []
	counter = 0
	chars = ''

	# Loop through the predicted text characters and find the ones that match the median text height
	for pred in characters:
		ht = hts1[counter]
		h = ht[0][0]
		removed = False
		if h < round(highest - 5) and type == 0:
			chars = chars + pred + '-'
			removed = True
		if h < round(highest - 12) and type == 1:
			chars = chars + pred + '-'
			removed = True
		if removed != True:
			finalHeights.append(ht)
			finalCharacters.append(pred)
		counter += 1

	# counter = 0
	# height = 0
	# # Calculate height median of the text characters
	# for ht in hts:
	# 	h = ht
	# 	height = height + h
	# 	counter += 1
	# medianH = round(height/counter)

	# counter = 0
	# heights = []
	# characters = []
	# chars = ''
	# for pred in predictions:
	# 	ht = hts[counter]
	# 	h = ht
	# 	if h < medianH - 5:
	# 		chars = chars + pred + '-'
	# 	else:
	# 		heights.append(ht)
	# 		characters.append(pred)
	# 	counter += 1
			
	prediction = ''
	for pred in finalCharacters:
		prediction = prediction + pred

	return prediction